<?php

require_once('../application/core/Model.php');
require_once('../application/core/db.php');
require_once('../application/models/ModelRegistration.php');

$login = $_GET['login'];
$password = $_GET['password'];
$password2 = $_GET['password2'];

$loginModel = new ModelRegistration();
$logins = $loginModel->getAllRows();

foreach($logins as $log){
    if ($login == $log['login']) {
        echo 'Такой логин существует ';
        break;
    }
}

if ($password != $password2)
    echo 'пароли не совпадают ';

if ($password == '' || $password2 == '' || $login == '')
    echo 'Не заполнены поля ';

