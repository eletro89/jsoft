<?php
require_once('../application/core/Model.php');
require_once('../application/core/db.php');
require_once('../application/models/ModelWeight.php');


$weightId = $_GET['weightId'];
$managerId = $_GET['managerId'];
$select = array(
    'where' => 'id = ' . $weightId,
);
$weight = new ModelWeight($select);

$weight->id = $weightId;
$weight->manager_id = $managerId;

$weight->update();
