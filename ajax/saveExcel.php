<?php

require_once('../classes/PHPExcel.php');
require_once('../classes/PHPExcel/Cell/AdvancedValueBinder.php');
require_once('../application/core/Model.php');
require_once('../application/core/db.php');
require_once('../application/models/ModelWeight.php');
require_once('../libs/functions.php');

$userId = $_GET['userId'];
$userRole = $_GET['userRole'];

PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );
$pExcel = new PHPExcel();
$pExcel->setActiveSheetIndex(0);
$aSheet = $pExcel->getActiveSheet();
$aSheet->setTitle('Первый лист');

$weightModel = new ModelWeight();
$weightList = $weightModel->getUserWeights($userRole . 's', $userId);


$aSheet->setCellValue('A1', 'Груз');
(isManager()) ? $role = 'Клиент' : $role = 'Менеджер';
$aSheet->setCellValue('B1', $role);
$aSheet->setCellValue('C1', 'Дата прибытия');
$aSheet->setCellValue('D1', 'Статус');

$aSheet->getColumnDimension('B')->setWidth(60);
$aSheet->getColumnDimension('C')->setWidth(50);
$aSheet->getColumnDimension('D')->setWidth(50);

$i = 2;
while ($weight = $weightList->fetch()) {
    $aSheet->setCellValue("A$i", $weight['container']);
    if (!isManager())
        $aSheet->setCellValue("B$i", $weight['managerName']);
    else
        $aSheet->setCellValue("B$i", $weight['clientName']);

    $aSheet->setCellValue("C$i", $weight['arrival_date']);
    $aSheet->setCellValue("D$i", $weight['status']);

    $i++;
}
$objWriter = PHPExcel_IOFactory::createWriter($pExcel, 'Excel2007');

$currDate = date("Y-m-d");
$filename = $userRole . '_' . $userId . '_' . $currDate . '.xlsx';

$objWriter->save($filename);
?>

<p style="padding-top: 50px; padding-left: 20px; font-size: 16px;" id="file_name">файл создан <?=$filename?></p>
<input type="button" value="Сохранить" style="margin-left: 60px; height: 30px; border-radius: 10px" onclick="location.href='/ajax/<?=$filename?>'; $('#saveEx').css('display', 'none')" >
<input type="button" value="Отправить по почте" style="margin-left: 60px; height: 30px; border-radius: 10px" onclick="sendExcel('<?=$filename?>', '<?=$userRole?>', '<?=$userId?>');" >
<input type="button" value="Закрыть" style="height: 30px; border-radius: 10px" onclick="$('#saveEx').css('display', 'none')" >




