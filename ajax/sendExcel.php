<?php

require_once('../application/core/Model.php');
require_once('../application/core/db.php');
require_once('../application/models/ModelClient.php');
require_once('../application/models/ModelManager.php');

require_once('../lib/class1.phpmailer.php');

$userRole = $_GET['userRole'];
$userId = $_GET['userId'];
$filename = $_GET['filename'];

$select = array(
    'where' => 'id=' . $userId,
);

switch($userRole){
    case 'managers':
        $model = new ModelManager($select);
        $model = $model->getOneRow();
        break;
    case 'clients':
        $model = new ModelClient($select);
        $model = $model->getOneRow();
        break;
}



$email = $model['email'];

$mail = new PHPMailer();
$mail->IsSendmail();
$mail->CharSet = "Windows-1251";
$mail->IsHTML(true);
$mail->AddReplyTo("noreply@altavina.ru", 'Альта Вина');
$mail->SetFrom("noreply@altavina.ru", 'Альта Вина');

$mail->AddAddress($email, "");
$mail->Subject = 'jsoft';
$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";

$mail->MsgHTML('jsoft');
if ($filename != '') {
    $mail->AddAttachment('' . $filename, $filename, "base64", 'xlsx');
}

if (!$mail->Send()) {
    echo 'Письмо не отправлено';
}
else
    echo 'Письмо отправлено';


