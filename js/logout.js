function logout(){
    $.ajax({
        url: "/ajax/logout.php",
        success: function(data){
            //$("#div_login").css('display', 'block');
            //$("#div_logout").css('display', 'none');
            //$("#new_weights").css('display', 'none');
            //$("#user_panel").css('display', 'none');
            location.href = '/';
        },
        error: function(XMLHttpRequest, type_err, except){
            alert("Ошибка ajax\r\n"+except + "\r\n" + type_err + "\r\n" + XMLHttpRequest);
        }
    });
}
