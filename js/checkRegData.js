function checkData(){
    var login = $("#userLogin").val();
    var password = $("#password").val();
    var password2 = $("#password2").val();
    $.ajax({
        url: "/ajax/checkRegData.php",
        data: {login: login, password: password, password2: password2},
        success: function(data){
            if (data == '')
                $("#reg_form").submit();
            else
                alert(data);
        },
        error: function(XMLHttpRequest, type_err, except){
            alert("Ошибка ajax\r\n"+except + "\r\n" + type_err + "\r\n" + XMLHttpRequest);
        }
    });
}

function checkUser(){
    var login = $("#login").val();
    var password = $("#password").val();
    $.ajax({
        url: "/ajax/checkUser.php",
        data: {login: login, password: password},
        success: function(data){
            if (data == '')
                $("#login_form").submit();
            else
                alert(data);
        },
        error: function(XMLHttpRequest, type_err, except){
            alert("Ошибка ajax\r\n"+except + "\r\n" + type_err + "\r\n" + XMLHttpRequest);
        }
    });
}


