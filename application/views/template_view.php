<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>jsoft</title>

    <link rel="stylesheet" type="text/css" href="/css/style.css" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="/js/saveExcel.js" type="text/javascript"></script>
    <script src="/js/manager.js" type="text/javascript"></script>
    <script src="/js/logout.js" type="text/javascript"></script>
    <script src="/js/checkRegData.js" type="text/javascript"></script>

</head>

<body>
<header>
    <div id="top-menu" class="MenuWrap">
        <?if (isset($_SESSION['userRole'])){?>
            <a href="/client"  class="ListItem">Клиенты</a>
            <a href="/manager"  class="ListItem">Менеджеры</a>

            <div id="user_panel" style="width: 300px">
                <?if (isManager()){?>
                    <a href="/weight/new/1" class="ListItem" id="new_weights">Новые грузы</a>
                <?}?>
                <a href="/" class="ListItem" id="new_weights">Моя страница</a>
            </div>
        <?}?>
        <div id="div_login" <?if (isset($_SESSION['userId'])){?>style="display: none"<?}?> style="width: 400px">
            <a href="/manager/add"  class="ListItem">Регистрация(м)</a>
            <a href="/client/add"  class="ListItem">Регистрация(кл)</a>
            <a href="/registration/login"  class="ListItem">вход</a>
        </div>

        <div id="div_logout" <?if (!isset($_SESSION['userId'])){?>style="display: none"<?}?> onclick="logout()">
            <a href="javascript:void(0)"  class="ListItem">выход</a>
        </div>

    </div>
</header>

<?php include 'application/views/'.$contentView; ?>
</body>
</html>