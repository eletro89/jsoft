<h1> Редактировать груз <?=$data['container']?></h1>
<form id="edit_container" method="post" action="/weight/update">
    <input name="id" value="<?=$data['id']?>" hidden="hidden">
    <label>Дата:</label><input type="date" name="arrival_date" value="<?=$data['arrival_date']?>">
    <label>Статус:</label><select name="status">
        <?foreach ($data['statusArr'] as $key => $val){?>
            <option value="<?=$val?>"><?=$val?></option>
        <?}?>

    </select>
    <p><input type="submit" value="Сохранить"></p>
</form>
