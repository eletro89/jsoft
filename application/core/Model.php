<?php

Abstract Class Model
{

    protected $db;
    protected $table;
    private $dataResult;

    public function __construct($select = false)
    {
        $this->db = db::getInstance();

// имя таблицы
        $modelName = get_class($this);
        $tableName = $this->getTableName();
        $this->table = $tableName;

// обработка запроса, если нужно
        $sql = $this->_getSelect($select);

        $sel = "SELECT * FROM $this->table";
        if ($sql)
            $sel .= $sql;

        $this->_getResult($sel);
    }

// получить имя таблицы
    public function getTableName()
    {
        return $this->table;
    }

    public function getName(){ //имя модели
        $name = $this->table;
        $name = substr($name, 0, -1); // удаляем s
        return $name;
    }

    public function isTempUser(){
        $table =  self::getTableName();
        $role = substr($table, 0, -1);
        if (isset($_SESSION['userId']) && $_SESSION['userId'] == $this->dataResult[0]['id'] && $_SESSION['userRole'] == $role)
            return true;
        return false;
    }

// получить все записи
    function getAllRows()
    {
        if (!isset($this->dataResult) OR empty($this->dataResult)) return false;
        return $this->dataResult;
    }

// получить одну запись
    function getOneRow()
    {
        if (!isset($this->dataResult) OR empty($this->dataResult)) return false;
        return $this->dataResult[0];
    }

// извлечь из базы данных одну запись
    function fetchOne()
    {
        if (!isset($this->dataResult) OR empty($this->dataResult)) return false;
        foreach ($this->dataResult[0] as $key => $val) {
            $this->$key = $val;
        }
        return true;
    }

// получить запись по id
    function getRowById($id)
    {
        try {
            $db = $this->db;
            $stmt = $db->query("SELECT * from $this->table WHERE id = $id");
            $row = $stmt->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
        return $row;
    }

// запись в базу данных
    public function save() {
        $arrayAllFields = array_keys($this->fieldsTable());
        $arraySetFields = array();
        $arrayData = array();

        $strInsert = "'";
        foreach($arrayAllFields as $field){
            if(!empty($this->$field)){
                $arraySetFields[] = $field;
                $strInsert .= $this->$field . "','";
                $arrayData[] = $this->$field;
            }
        }
        $strInsert = substr($strInsert, 0, -2);
        $forQueryFields =  implode(', ', $arraySetFields);

        try {
            $db = db::getInstance()->dbh;
             $stmt = $db->prepare("INSERT INTO $this->table ($forQueryFields) values ($strInsert)");
            $stmt->execute($arrayData);
            $result = $db->lastInsertId();
        }catch(PDOException $e){
            echo 'Error : '.$e->getMessage();
            echo "<br/>Error sql : " . "INSERT INTO $this->table ($forQueryFields) values ($strInsert)";
            exit();
        }

        return $result;
    }

// составление запроса к базе данных
    private function _getSelect($select)
    {
        if (is_array($select)) {
            $allQuery = array_keys($select);
            array_walk($allQuery, function (&$val) {
                $val = strtoupper($val);
            });

            $querySql = "";
            if (in_array("WHERE", $allQuery)) {
                foreach ($select as $key => $val) {
                    if (strtoupper($key) == "WHERE") {
                        $querySql .= " WHERE " . $val;
                    }
                }
            }

            if (in_array("GROUP", $allQuery)) {
                foreach ($select as $key => $val) {
                    if (strtoupper($key) == "GROUP") {
                        $querySql .= " GROUP BY " . $val;
                    }
                }
            }

            if (in_array("ORDER", $allQuery)) {
                foreach ($select as $key => $val) {
                    if (strtoupper($key) == "ORDER") {
                        $querySql .= " ORDER BY " . $val;
                    }
                }
            }

            if (in_array("LIMIT", $allQuery)) {
                foreach ($select as $key => $val) {
                    if (strtoupper($key) == "LIMIT") {
                        $querySql .= " LIMIT " . $val;
                    }
                }
            }
            return $querySql;
        }
        return false;
    }

// выполнение запроса к базе данных
    private function _getResult($sql)
    {
        try {
            $db = $this->db->dbh;
//echo $sql; die;
            $stmt = $db->query($sql);
            $rows = $stmt->fetchAll();
            $this->dataResult = $rows;
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $rows;
    }

// обновление записи. Происходит по ID
    public function update()
    {
        $arrayAllFields = array_keys($this->fieldsTable());
        $arrayForSet = array();
        foreach ($arrayAllFields as $field) {
            if (!empty($this->$field)) {
                if (strtolower($field) != 'id') {
                    $arrayForSet[] = $field . ' = "' . $this->$field . '"';
                } else {
                    $whereID = $this->$field;
                }
            }
        }
        if (!isset($arrayForSet) OR empty($arrayForSet)) {
            echo "Array data table `$this->table` empty!";
            exit;
        }
        if (!isset($whereID) OR empty($whereID)) {
            echo "ID table `$this->table` not found!";
            exit;
        }

        $strForSet = implode(', ', $arrayForSet);

        try {
            $db = $this->db->dbh;
            $stmt = $db->prepare("UPDATE $this->table SET $strForSet WHERE `id` = $whereID");
            $result = $stmt->execute();
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            echo '<br/>Error sql : ' . "'UPDATE $this->table SET $strForSet WHERE `id` = $whereID'";
            exit();
        }
        return $result;
    }
}