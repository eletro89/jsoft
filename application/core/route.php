<?php

class Route
{
    static function start()
    {
        // контроллер и действие по умолчанию
        if (isset($_SESSION['userRole'])) {
            $controllerName = ucwords($_SESSION['userRole']);
            $actionName = 'view';
            $id = $_SESSION['userId'];
        }
        else{
            $controllerName = 'Registration';
            $actionName = 'login';
        }

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // получаем имя контроллера
        if (!empty($routes[1])) {
            $controllerName = $routes[1];
                // получаем имя экшена
            if (!empty($routes[2])) {
                $actionName = $routes[2];
            }
            else{
                $actionName = 'index';
            }

            if (!empty($routes[3])){
                $id = $routes[3];
            }
        }



        // добавляем префиксы
        $modelName = 'Model' . $controllerName;
        $controllerName = 'Controller' . $controllerName;
        $actionName = 'action_' . $actionName;

        // подцепляем файл с классом модели (файла модели может и не быть)

        $modelFile = strtolower($modelName) . '.php';
        $modelPath = "application/models/" . $modelFile;
        if (file_exists($modelPath)) {
            include "application/models/" . $modelFile;
        }

        // подцепляем файл с классом контроллера
        $controllerFile = strtolower($controllerName) . '.php';
        $controllerPath = "application/controllers/" . $controllerFile;

        if (file_exists($controllerPath)) {
            include "application/controllers/" . $controllerFile;
        } else {
            Route::ErrorPage404();
        }

        // создаем контроллер
        $controller = new $controllerName;
        $action = $actionName;

        if (method_exists($controller, $action)) {
// вызываем действие контроллера
            if (($action == 'action_add' || 'action_view' || 'action_edit') && $id != ''){
                $controller->$action($id);
            }
            else {
                $controller->$action();
            }
        } else {
            Route::ErrorPage404();
        }

    }

    function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}