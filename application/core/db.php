<?php
class db {
    protected static $_instance;

    public $dbh;
    /*
     * Connect to data base
     */
    private function __construct(){
        try {
            $this->dbh = new PDO('mysql:host=localhost;dbname=jsoft;charset=utf8;', 'jsoft', 'jsoft1');
            $this->dbh->exec('SET CHARACTER SET utf8');

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }


    }


    /*
    * Return count or raws in table
    */
    public function count($table, $where = ''){

        $sql = "SELECT COUNT(*) FROM ".$table;
        if ($where <> ""){
            $sql .= ' WHERE '.$where;
        }

        if(strripos($where, 'GROUP BY') !== false){//Если был передан параметрв GROUP BY - будет возврщен не 1 результат
            $rs = $this->dbh->query($sql);
            return $rs->rowCount();
        }

        $q = $this->dbh->query($sql);
        $res = $q->fetch();

        return $res;
    }

    /*
    * Return raws obj
    */
    public function fetchAll($table, $select='*', $where = '', $order = '', $group_by='', $limit_from = -1, $limit_to = -1){

        if ($limit_from == ''){
            $limit_from = -1;
        }
        if ($limit_to == ''){
            $limit_to = -1;
        }

        if ($limit_to != -1){
            if ($limit_from == -1){
                $limit_from = 0;
            }
        }

        $res_ar = array();

        $sql = "SELECT $select FROM ".$table;

        if ($where <> ''){
            $sql .= ' WHERE '.$where;
        }
        if($group_by != ''){
            $sql .= " GROUP BY $group_by ";
        }
        if ($order <> ''){
            $sql .= ' ORDER BY '.$order;
        }
        if ($limit_from != -1){
            if($limit_from < 0){
                $limit_from = 0;
            }
            $sql .= ' LIMIT '.$limit_from;
        }
        if ($limit_to != -1){
            $sql .= ',' . $limit_to;
        }

        $q = $this->dbh->query($sql);
        if ($q->rowCount() <> 0){
//            while($res = $q->fetch()){
//                $res_ar[] = $res;
//            }
            $res = $q;
            return $res;
        }
        return false;
    }

    public function fetchRaw($table, $where='', $order=''){
        //$sql = "SELECT * FROM `".$table . "` ";
        $sql = "SELECT * FROM ".$table." ";
        if ($where <> ''){
            $sql .= ' WHERE '.$where;
        }
        if ($order <> ''){
            $sql .= ' ORDER BY '.$order;
        }
        $sql .= ' LIMIT 1;';

        $res_ar = array();
        $q = $this->dbh->query($sql);
        if ($q->rowCount() <> 0){
            $res_ar = $q->fetch();
            return $res_ar;
        }
        else{
            return false;
        }
    }

    public function saveRaw($table, $data){
        for($count=0;$count < (count($data)); $count++){
            $keys[] = key($data);
            next($data);
        }

        $sql = 'INSERT INTO '.$table.' (';
        $count = 0;
        foreach($keys as $key){
            $count++;
            if ($count <> 1){
                $sql .= ',';
            }
            $sql .= '`'.$key.'`';
        }
        $sql .= ') VALUES (';
        $count = 0;
        foreach ($data as $value){
            $count++;
            if ($count <> 1){
                $sql .= ',';
            }
            $sql .= "'".htmlspecialchars(trim($value), ENT_QUOTES)."'";
        }
        $sql .= ');';
        $q = $this->dbh->query($sql);

        if(!$q){
            return false;
        }
        return mysql_insert_id();
    }

    public function update($table, $data, $where, $limit=0){

        for($count=0; $count < (count($data)); $count++){
            $keys[] = key($data);
            next($data);
        }

        $count = -1;
        $sql = 'UPDATE '.$table.' SET ';

        foreach($data as $d){
            $count++;;
            if ($count <> 0)
                $sql .= ',';
            $sql .= '`'.$keys[$count].'`='."'".htmlspecialchars(trim($d), ENT_QUOTES)."'";
        }
        $sql .= ' WHERE ' . $where;

        if ($limit <>0)
            $sql .= ' LIMIT ' . $limit;

        $q = $this->dbh->query($sql);

        return $q;
    }




    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}