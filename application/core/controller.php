<?php

class Controller {

    public $model;
    public $view;

    function __construct()
    {
        $this->view = new View();

    }

    function errorValidate(){
        $this->view->generate('error_validate.php', 'template_view.php');
    }


}