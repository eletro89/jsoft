<?php

require_once('/application/models/ModelRegistration.php');

class ModelManager extends ModelRegistration
{
    public $table = 'managers';
    public $primary = 'id';

    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'email',
            'phone' => 'Телефон',
        );
    }


    public function validate($post){
        if (preg_match('/[^(\w)|(\@)|(\.)|(\-)]/', $post['email']))
            return false;
        return true;
    }

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}