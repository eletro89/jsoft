<?php

require_once('/application/models/ModelRegistration.php');

class ModelClient extends ModelRegistration
{
    public $table = 'clients';
    public $primary = 'id';

    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'name' => 'name',
            'inn' => 'inn',
            'address' => 'address',
            'email' => 'email',
            'phone' => 'phone',
        );
    }


    public function validate($post){
        if (!preg_match('/((8|\+7)-?)?\(?\d{3,5}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/', $post['phone']))
            return false;
        if (preg_match('/[^(\w)|(\@)|(\.)|(\-)]/', $post['email']))
            return false;
        return true;
    }

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}