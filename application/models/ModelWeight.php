<?php

class ModelWeight extends Model
{
    public $table = 'weights';
    public $id;
    public $client_id;
    public $container;
    public $manager_id;
    public $arrival_date;
    public $status;

    public $statusArr = array(
        'awaiting' => 'AWAITING',
        'onboard' => 'On Board',
        'finished' => 'Finished'
    );

    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'container' => 'Container',
            'client_id' => 'client_id',
            'manager_id' => 'manager_id',
            'arrival_date' => 'arrival_date',
            'status' => 'status',
        );
    }

    public function isNew(){
        return ' manager_id = 0';
    }

    public function getWeights($isNew='')
    {
        $sql = "SELECT w.id, container, client_id, manager_id, arrival_date, status,  m.name as managerName, cl.name as clientName
                                                FROM weights w
                                                LEFT JOIN managers m ON manager_id = m.id
                                                LEFT JOIN clients cl ON client_id = cl.id";
        if ($isNew == 1){
            $sql .= " WHERE " . $this->isNew();
        }

        $data = db::getInstance()->dbh->query($sql);

        return $data;
    }

    public function getUserWeights($userRole, $userId)
    {
        $sql = "SELECT weights.id, container, arrival_date, status, client_id, manager_id, arrival_date, managers.name as managerName, clients.name as clientName
                                                FROM weights
                                                LEFT JOIN managers  ON manager_id = managers.id
                                                LEFT JOIN clients ON client_id = clients.id
                                                WHERE $userRole.id = $userId";

        $data = db::getInstance()->dbh->query($sql);


        return $data;
    }


    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}