<?php


class ModelRegistration extends Model
{
    public $table = 'registrations';
    public $id = 'id';
    public $role = 'role';
    public $user_id = 'user_id';
    public $login = 'login';
    public $password = 'password';

    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'role' => 'role',
            'user_id' => 'User id',
            'login' => 'login',
            'password' => 'password',
        );
    }



    public function run() {
        $login = $_POST['login'];
        $passwd = md5($_POST['password']);
        if ($login != '' && $passwd != '') {
            $sth = $this->db->dbh->prepare("SELECT user_id, role FROM registrations WHERE login = '$login' AND password = '$passwd'");
            $sth->execute();

            $data = $sth->fetchAll();
            $data = $data[0];
            $count = $sth->rowCount();
            if ($count > 0) {
                $_SESSION['userId'] = $data['user_id'];
                $_SESSION['userRole'] = $data['role'];
            }
        }
    }

    public function validate($post){
        if ($post['password'] != $post['password2'])
            return false;

        return true;
    }

    public function saveUser($role, $userId, $post){
        $this->user_id = $userId;
        $this->role = $role;
        $this->login = $post['login'];
        $passwd = md5($post['password']);
        $this->password = $passwd;
        $this->save();
        $_SESSION['userId'] = $userId;
        $_SESSION['userRole'] =  $role;
    }



    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}