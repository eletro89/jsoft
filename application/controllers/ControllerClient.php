<?php

require_once("/application/models/ModelWeight.php");

class ControllerClient extends Controller
{

    function __construct()
    {
        $this->model = new ModelClient();
        $this->view = new View();
    }

    function action_index()
    {
        $data = new ModelClient();
        $data = $data->getAllRows();
        $this->view->generate('client/index.php', 'template_view.php', $data);
    }

    function action_view($id)
    {

        $selectClient = array(
            'where' => 'id = ' . $id,
        );
        $model = new ModelClient($selectClient);
        $data = $model->getOneRow();
        $weight = new ModelWeight();
        $data['weights'] = $weight->getUserWeights($this->model->table, $id);
        $data['userRole'] = $this->model->table;
        $data['isTemplUser'] = $model->isTempUser();
        $this->view->generate('client/view.php', 'template_view.php', $data);
    }

    function action_add()
    {
        $modelClient = new ModelClient();
        $client['fields'] = $modelClient->fieldsTable();
        $this->view->generate('client/add.php', 'template_view.php', $client);
    }

    function action_save(){
        $modelClient = new ModelClient();
        $modelRegistr = new ModelRegistration();
        if ($modelRegistr->validate($_POST) && $modelClient->validate($_POST)) {
            $setFields = $modelClient->fieldsTable();
            foreach ($setFields as $key => $value) {
                $modelClient->$key = $_POST[$key];
            }
            $client_id = $modelClient->save();
            $modelRegistr->saveUser('client', $client_id, $_POST);

            $this->action_view($client_id);
        }
        else {
            $this->errorValidate();
        }
    }
}