<?php

require_once("/application/models/ModelManager.php");
require_once("/application/models/ModelClient.php");

class ControllerWeight extends Controller
{

    function __construct()
    {
        $this->model = new ModelWeight();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->getWeights();
        $this->view->generate('weight/index.php', 'template_view.php', $data);
    }


    function action_add($id){
        $data = $this->model->getWeights();
        $this->view->generate('weight/add.php', 'template_view.php', $data);
    }

    function action_save(){
        $weight = new ModelWeight();
        $weight->client_id = $_SESSION['userId'];
        $weight->container = $_POST['container'];
        $weight->manager_id = '0';
        $weight->status = $weight->statusArr['awaiting'];
        $weight->save();

        $select = array(
            'where' => 'id =' . $_SESSION['userId'],
        );
        if (isManager())
            $model = new ModelManager($select);
        else
            $model = new ModelClient($select);

        $data = $model->getOneRow();
        $weight = new ModelWeight();
        $data['weights'] = $weight->getUserWeights($model->getTableName(), $data['id']);
        $data['userRole'] = $_SESSION['userRole'];
        $data['isTemplUser'] = $model->isTempUser();
        $this->view->generate($model->getName() . '/view.php', 'template_view.php', $data);
    }

    function action_update(){
        $selectWeight = array(
            'where' => 'id = ' . $_POST['id']
        );
        $weight = new ModelWeight($selectWeight);

        $arrayAllFields = array_keys($weight->fieldsTable());
        foreach ($arrayAllFields as $field) {
            if (!empty($_POST[$field])) {
                $weight->$field = $_POST[$field];
            }
        }

        $weight->update();
        $data = $this->model->getWeights();
        $this->view->generate('weight/index.php', 'template_view.php', $data);
    }

    function  action_new($id){
        $data = $this->model->getWeights(1);
        $data->manager = $id;
        $this->view->generate('weight/new.php', 'template_view.php', $data);
    }

    function  action_edit($id){
        $selectWeight = array(
            'where' => 'id=' . $id,
        );
        $model = new ModelWeight($selectWeight);
        $data = $model->getOneRow();
        $data['statusArr'] = $model->statusArr;
        $this->view->generate('weight/edit.php', 'template_view.php', $data);
    }


}