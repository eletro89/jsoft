<?php

require_once("/application/models/ModelWeight.php");
require_once("/application/models/ModelManager.php");
require_once("/application/models/ModelClient.php");

class ControllerRegistration extends Controller
{

    function __construct()
    {
        $this->model = new ModelRegistration();
        $this->view = new View();
    }

    function action_login()
    {
        $this->view->generate('registration/login.php', 'template_view.php');
    }

    function action_run(){
        $this->model->run();

        $select = array(
            'where' => 'id =' . $_SESSION['userId'],
        );
        if (isManager())
            $model = new ModelManager($select);
        else
            $model = new ModelClient($select);

        $data = $model->getOneRow();
        $weight = new ModelWeight();
        $data['weights'] = $weight->getUserWeights($model->getTableName(), $data['id']);
        $data['userRole'] = $_SESSION['userRole'];
        $data['isTemplUser'] = $model->isTempUser();
        $this->view->generate($model->getName() . '/view.php', 'template_view.php', $data);
    }


}