<?php

require_once("/application/models/ModelWeight.php");
require_once("/application/models/ModelRegistration.php");

class ControllerManager extends Controller
{

    function __construct()
    {
        $this->model = new ModelManager();
        $this->view = new View();
    }

    function action_index()
    {
        $model = new ModelManager();
        $data = $model->getAllRows();
        $this->view->generate('manager/index.php', 'template_view.php', $data);
    }

    function action_view($id)
    {
        $selectManager = array(
            'where' => 'id = ' . $id,
        );
        $model = new ModelManager($selectManager);
        $data = $model->getOneRow();
        $weight = new ModelWeight();
        $data['weights'] = $weight->getUserWeights($this->model->table, $id);
        $data['userRole'] = $this->model->table;
        $data['isTemplUser'] = $model->isTempUser();
        $this->view->generate('manager/view.php', 'template_view.php', $data);
    }

    function action_add()
    {
        $modelManager = new ModelManager();
        $manager['fields'] = $modelManager->fieldsTable();
        $this->view->generate('manager/add.php', 'template_view.php', $manager);
    }


    function action_save()
    {
        $modelRegistr = new ModelRegistration();
        $modelManager = new ModelManager();
        if ($modelRegistr->validate($_POST) && $modelManager->validate($_POST)){

            $setFields = $modelManager->fieldsTable();
            foreach ($setFields as $key => $value) {
                $modelManager->$key = $_POST[$key];
            }
            $manager_id = $modelManager->save();

            $modelRegistr->saveUser('manager', $manager_id, $_POST);

            $this->action_view($manager_id);
        }
        else {
            $this->errorValidate();
        }
    }
}